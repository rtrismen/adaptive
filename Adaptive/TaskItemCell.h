//
//  TaskItemCell.h
//  Adaptive
//
//  Created by Richard Trismen on 1/10/18.
//  Copyright © 2018 Allego. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *itemLabel;

@end
