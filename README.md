# README #

The Adaptive Project.

### What is this repository? ###

* Prototyping project associated with ABT-7806

### What is its purpose? ###

This project illustrates how to use the following, while stubbing out UI for the new iOS Allego 5.0
Task View:

* Adaptive AutoLayout
* Size classes
* Font size variation
* Installed and Uninstalled Constraints
* Changing the position of UI elements when Size Class changes
* Completely removing/adding pieces of UI when Size Class changes

It also illustrates how to use a storyboard reference to segue from a legacy storyboard to a 
new storyboard that uses Size Classes.

### Who do I talk to about this project? ###

* Richard Trismen