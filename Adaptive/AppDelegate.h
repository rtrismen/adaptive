//
//  AppDelegate.h
//  Adaptive
//
//  Created by Richard Trismen on 1/8/18.
//  Copyright © 2018 Allego. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

