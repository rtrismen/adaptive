//
//  ViewController.m
//  Adaptive
//
//  Created by Richard Trismen on 1/8/18.
//  Copyright © 2018 Allego. All rights reserved.
//

#import "ViewController.h"
#import "TaskItemCell.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UICollectionView *taskListCollectionView;
@property (strong, nonatomic) NSArray *tasks;

@end

@implementation ViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _tasks = @[@"item name 1", @"item name 2", @"item name 3", @"item name 4"];

    //[self.taskListCollectionView registerClass:[TaskItemCell class] forCellWithReuseIdentifier:@"TaskItemCell"];
    [self.taskListCollectionView reloadData];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma UICollectionViewDataSource

- (NSInteger) collectionView:(UICollectionView *)collectionView
      numberOfItemsInSection:(NSInteger)section
{
    return self.tasks.count;
}

- (__kindof UICollectionViewCell *) collectionView:(UICollectionView *)collectionView
                            cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TaskItemCell";
    TaskItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                                   forIndexPath:indexPath];
    // Get the task item name:
    NSString *text = [self.tasks objectAtIndex:indexPath.row];
    
    // Populate the cell:
    cell.itemLabel.text = text;
    cell.backgroundColor = [UIColor yellowColor];
    
    return cell;
}

@end
