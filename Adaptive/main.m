//
//  main.m
//  Adaptive
//
//  Created by Richard Trismen on 1/8/18.
//  Copyright © 2018 Allego. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
